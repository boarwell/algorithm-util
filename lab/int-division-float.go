package main

import "fmt"

func main() {
	x, y := 10, 3
	f, g := 10.0, 3.0
	fmt.Println("int:", x/y)
	fmt.Println("float:", f/g)
}
