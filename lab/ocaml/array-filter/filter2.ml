let filter2 p ar =
  let len = Array.length ar in
  let ar' = Array.make len None in
  for i = len - 1 downto 0 do
    if p ar.(i) then ar'.(i) <- Some ar.(i) else ar'.(i) <- None
  done ;
  ar'

let ar = Array.init 10000000 (fun x -> x)
let p x = x mod 2 = 0
let () =
  Array.iter 
    (function Some x -> ignore x | None -> ()) 
    (filter2 p ar)