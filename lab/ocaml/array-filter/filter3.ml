let filter3 p ar =
  let len = Array.length ar in
  let rec aux i tmp = 
    if i < 0 then tmp
    else if p ar.(i) then aux (i - 1) (i :: tmp) else aux (i - 1) tmp
  in
  let ls = aux (len - 1) [] in
  let ar' = Array.make (List.length ls) ar.(0) in
  let rec aux' i = function
    | h :: t -> ar'.(i) <- ar.(h); aux' (i + 1) t
    | [] -> ()
  in
  aux' 0 ls ; ar'


let ar = Array.init 10000000 (fun x -> x)
let p x = x mod 2 = 0
let () = Array.iter ignore (filter3 p ar)