let filter1 p ar =
  let len = Array.length ar in
  let rec aux i tmp = 
    if i < 0 then Array.of_list tmp
    else if p ar.(i) then aux (i - 1) (ar.(i) :: tmp)
    else aux (i - 1) tmp
  in
  aux (len - 1) []

let ar = Array.init 10000000 (fun x -> x)
let p x = x mod 2 = 0
let () = Array.iter ignore (filter1 p ar)