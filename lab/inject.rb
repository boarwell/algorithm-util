numbers = [4, 3, 9, 8, 5, 6, 1, 7, 2]

p numbers.inject(1) { |ans, x| ans * x}
p numbers.inject { |sum, x| sum + x}
p numbers.inject(:+)
