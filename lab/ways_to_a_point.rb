# 最強最速アルゴリズマー養成講座 - p169

@h = 14
@w = 14
m = Array.new(@h, Array.new(@w, 0))
@memo = Hash.new(0)

def dfs(now_h, now_w)
  return 0 if (now_h > @h) || (now_w > @w)
  return 1 if (now_h == @h) || (now_w == @w)
  dfs(now_h + 1, now_w) + dfs(now_h, now_w + 1)
end

def dfs_memo(now_h, now_w)
  return 0 if (now_h > @h) || (now_w > @w)
  return 1 if (now_h == @h) || (now_w == @w)
  return @memo[[now_h, now_w]] if @memo.has_key?([now_h, now_w])
  @memo[[now_h, now_w]] = dfs(now_h + 1, now_w) + dfs(now_h, now_w + 1)
end

p dfs_memo(0, 0)
