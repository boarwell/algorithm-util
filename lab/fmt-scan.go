package main

import "fmt"

var (
	a, b, c, d int
)

// byScanln(), byScanf()ともに
// 12 13
// 14 15
// という出力になる
func main() {
	byScanf()

	fmt.Println(a, b)
	fmt.Println(c, d)
}

func byScanln() {
	fmt.Scanln(&a, &b)
	fmt.Scanln(&c, &d)
}

func byScanf() {
	fmt.Scanf("%d%d\n%d%d", &a, &b, &c, &d)
}
