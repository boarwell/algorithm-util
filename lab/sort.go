package main

import (
	"fmt"
	"sort"
)

func main() {
	i := []int{1, 1, 1, 1}
	fmt.Println(sort.IsSorted(sort.IntSlice(i)))
}
