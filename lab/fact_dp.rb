def fact_dp(n)
  return 0 if n.zero?

  dp = []
  dp [0] = 1

  1.upto(n - 1) do |i|
    dp[i] = dp[i - 1] * (i + 1)
  end

  dp[n - 1]
end

def fact_rec(n)
  return 1 if n == 1
  n * fact_rec(n - 1)
end

# どちらも同じくらいの速度で計算できるので
# すっきり書ける再帰のほうがいいかも
# もしかして: 末尾最適化が効いてる？
puts fact_dp(10_000)
puts fact_rec(10_000)
