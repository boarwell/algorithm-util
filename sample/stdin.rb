# すべての行を取得する（1行ずつ処理できる）
while line = gets
  p line
end

# まとめて取得
#   splitは空白でも改行でも分割してくれる
#   linesは配列になる
#   style guideにはSTDINではなく$stdinを使う、とありました（標準入出力は$std-シリーズで）
lines = $stdin.read.split
