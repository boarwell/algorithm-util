// https://stackoverflow.com/questions/13870845/converting-from-an-integer-to-its-binary-representation

package sample

import (
	"fmt"
	"strconv"
)

func main() {
	n := int64(123)
	fmt.Println(
		"base2:", strconv.FormatInt(n, 2),
		"base8:", strconv.FormatInt(n, 8),
	)
}
